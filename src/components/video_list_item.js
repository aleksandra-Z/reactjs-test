import React from 'react';

const VideoItemList = ({video, onVideoSelect}) => { //tu w argumencie przed koniecznością wprowadzenia imageUrl było "props"
    // const video = props.video; to jest dokładnie to samo co od razu w argumencie zapisanie {video}
    const imageUrl = video.snippet.thumbnails.default.url;
    return ( 
            <li onClick={() => onVideoSelect(video)} className='list-group-item'>
                <div className="video-list media">
                    <div className="media-left">
                        <img classsName="media-object" src={imageUrl} />
                    </div>
                </div>
                <div className="media-body">
                    <div className='media-heading'>
                        {video.snippet.title}
                    </div>
                </div>
            </li>
    );
};

export default VideoItemList;