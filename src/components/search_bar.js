import React, { Component } from "react";

class SearchBar extends /* React.Component - możemy to napisać tak, albo przy importowaniu wyszczeglnic Component */ Component {
    constructor(props){
        super(props);
        this.state = {term:""};
    }
    
    render() {
        return (<div className='search-bar'>
                    <input 
                        value={this.state.term}
                        onChange={event => this.onInputChange(event.target.value)} />
                </div>);
    }
    
    onInputChange(term) {
        this.setState({term});
        this.props.onSearchTermChange(term);
    }
    
}
// powyzej proste handolwanie zdarzenia onChange(reactowe zdarzenie) poprzez funkcję "=>", w pewnym sensie anonimową. Inaczej to można zrobić jak poniżej - osobna funckja:


//class SearchBar extends /* React.Component - możemy to napisać tak, albo przy importowaniu wyszczeglnic Component */ Component {
//    render() {
//        return <input onChange={this.onInputChange}/>;
//    }
//    
//    onInputChange(event){
//      console.log(event.target.value);
//    }
//}

export default SearchBar;